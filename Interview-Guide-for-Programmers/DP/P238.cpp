/*
 * @Author: 缄默
 * @Date: 2021-12-14 20:53:28
 * @LastEditors: 缄默
 * @LastEditTime: 2021-12-14 21:16:43
 */

//数字字符串转换为字母组合的种树

#include <iostream>
#include <string>

using namespace std;

int num1(string& str);

int main() {
    string str = "1111";
    cout << num1(str) << endl;

    return 0;
}

//如果按照从前向后做，那么每次新加入一个字符都需要重新判断
//按照从后向前做，则不需要这么算
int num1(string& str) {
    if (str.size() == 0) return 0;
    int len = str.size() - 1;
    int cur = str[len] == '0' ? 0 : 1;
    int next = 1;
    int tmp = 0;
    for (int i = len - 1; i >= 0; i--) {
        if (str[i] == 0) {
            next = cur;
            cur = 0;
        }
        else {
            //如果不等于0了判断是否小于27是否可以有两种情况
            //一个是不组合的  一个是组合的
            tmp = cur;
            if ((str[i] - '0') * 10 + (str[i + 1] - '0') < 27) {
                cur += next;
            }
            next = tmp;
        }
    }
    return cur;
}
