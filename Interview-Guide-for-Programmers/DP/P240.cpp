/*
 * @Author: 缄默
 * @Date: 2021-12-14 21:17:22
 * @LastEditors: 缄默
 * @LastEditTime: 2022-03-16 19:54:55
 */

//表达式得到期望结果的组成种数

//TODO 补充解法
#include <iostream>
#include <string>

using namespace std;

int num1(string& str);

bool isValid(string& str);

int main() {
    string str = "1^0|0|1";
    cout << num1(str) << endl;

    return 0;
}

int num1(string& str) {
    if (!isValid(str)) return 0;
    return process1(str, 0, str.size() - 1);
}

bool isValid(string& str) {
    if (str.size() % 2 == 0) {
        for (int i = 0; i < str.size(); i = i + 2) {
            if (str[i] == '0' || str[i] == '1') {
                continue;
            }
            return false;
        }
        for (int i = 1; i < str.size(); i = i + 2) {
            if (str[i] == '|' || str[i] == '&' || str[i] == '^') {
                continue;
            }
            return false;
        }
        return true;
    }
    return false;
}

int process1(string& str, int left, int right) {
    int count = 0;
    if (right - left == 2) {
        switch (str[(right + left) / 2])
        {
        case '^' :
            count = (str[left] - '0') ^ (str[right] - '0');
            break;
        case '|' :
            count = (str[left] - '0') || (str[right] - '0');
            break;
        case '&' :
            count = (str[left] - '0') && (str[right] - '0');
            break;
        default:
            break;
        }
        return count;
    }
    for (int i = left + 1; i < right; i = i + 2) {
        count += process1(str, left, i + 1);
    }
}