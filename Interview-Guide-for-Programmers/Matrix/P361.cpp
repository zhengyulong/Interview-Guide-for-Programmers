#include <iostream>
#include <vector>

using namespace std;

class Solution {

public:
    //打印矩阵
    void rorateMatrix(const vector<vector<int>>& matrix) {
        int tR = 0;
        int tC = 0;
        int dR = matrix.size() - 1;
        int dC = matrix[0].size() - 1;
        while (tR <= dR && tC <= dC)
        {
            printMatrixEdge(matrix, tR++, tC++, dR--, dC--);
        }
        
    }

private:
    //打印矩阵的一圈
    void printMatrixEdge(const vector<vector<int>>& matrix, int tR, int tC, int dR, int dC) {
        if (tR == dR) {
            for (int i = tC; i <= dC; i++) {
                cout << matrix[tR][i] << " ";
            }
            cout << endl;
        } else if (tC == dC) {
            for (int i = tR; i <= dR; i++) {
                cout << matrix[i][tR] << " ";
            }
            cout << endl;
        } else {
            int curC = tC;
            int curR = tR;
            while (curC != dC) {
                cout << matrix[tR][curC] << " ";
                curC++;
            }
            while (curR != dR) {
                cout << matrix[curR][dC] << " ";
                curR++;
            }
            while (curC != tC) 
            {
                cout << matrix[dR][curC] << " ";
                curC--;
            }
            while (curR != tR)
            {
                cout << matrix[curR][tC] << " ";
                curR--;
            }                                  
        }

    }
};

int main() {
    vector<vector<int>> matrix {
        {1, 2, 3, 4},
        {5, 6, 7, 8},
        {9, 10, 11, 12},
        {13, 14, 15, 16}
    };
    Solution solu;
    solu.rorateMatrix(matrix);
    return 0;
    
}