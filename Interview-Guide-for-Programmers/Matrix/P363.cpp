#include <iostream>
#include <vector>

using namespace std;

class Solution {

public:
    //旋转矩阵
    void rorateMatrix(vector<vector<int>>& matrix) {
        if (matrix.size() == 0) return ;
        int tR = 0;
        int tC = 0;
        int dR = matrix.size() - 1;
        int dC = matrix[0].size() - 1;
        while (tR < dR)
        {
            rorateMatrixEdge(matrix, tR++, tC++, dR--, dC--);
        }
        
    }

    void printMatrix(const vector<vector<int>>& matrix) {
        for (int i = 0; i < matrix.size(); i++) {
            for (int j = 0; j < matrix[0].size(); j++) {
                cout << matrix[i][j] << " ";
            }
            cout << endl;
        }
    }
private:
    //旋转一层
    void rorateMatrixEdge(vector<vector<int>>& matrix, int tR, int tC, int dR, int dC) {
       int times = dC - tC;
       int tmp = 0;
       for (int i = 0; i != times; i++) {
           tmp = matrix[tR][tC + i];
           matrix[tR][tC + i] = matrix[dR - i][tC];
            matrix[dR - i][tC] = matrix[dR][dC - i];
            matrix[dR][dC - i] = matrix[tR + i][dC];
            matrix[tR + i][dC] = tmp;
       }

    }
};

int main() {
    vector<vector<int>> matrix {
        {1, 2, 3, 4},
        {5, 6, 7, 8},
        {9, 10, 11, 12},
        {13, 14, 15, 16}
    };
      vector<vector<int>> matrix2 {
        {1, 2, 3},
        {4, 5, 6},
        {9, 10, 11}
    };
    Solution solu;
    solu.rorateMatrix(matrix);

    solu.printMatrix(matrix);

    solu.rorateMatrix(matrix2);

    solu.printMatrix(matrix2);
    return 0;
    
}