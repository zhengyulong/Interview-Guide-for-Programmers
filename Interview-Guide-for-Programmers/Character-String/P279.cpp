
//数组中两个字符串的最小距离

#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution {
public:
    int minDistance(const vector<string>& strs, const string& s1, const string& s2) {
        if (s1.size() == 0 || s2.size() == 0) { return -1; }
        if (s1 == s2) {
            for (auto str : strs) {
                if (str == s1) { return 0; }
            }
        } else {
            int res = INT_MAX;
            int indexS1 = -1;
            int indexS2 = -1;
            for (int i = 0; i != strs.size(); i++) {
                if (strs[i] == s1) {
                    res = min(res, indexS2 == -1 ? res : i - indexS2);
                    indexS1 = i;
                    
                } else if (strs[i] == s2) {
                    res = min(res, indexS1 == -1 ? res : i - indexS1);
                    indexS2 = i;
                }
            }
            return res == INT_MAX ? -1 : res;
        }
        return -1;
    }
};

int main() {

    Solution solu;
    vector<string> strs({"qwe", "asd", "zxc"});
    cout << solu.minDistance(strs, "qwe", "zxc");
    return 0;
}