/*
 * @Author: 缄默
 * @Date: 2022-03-14 21:50:03
 * @LastEditors: 缄默
 * @LastEditTime: 2022-03-14 22:43:34
 */

//字符串的统计字符串

#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>

using namespace std;

string getCountString(string& s1);
int main() {
    string s1 = "aaabbc";
    cout << getCountString(s1) << endl;
    return 0;
}

string getCountString(string& s1) {
    char* head;
    int index = 1;
    if (s1.size() == 0) return head;
    int count = 0;
    *head = s1[0];

    //TODO 字符串拼接仍需要熟练
    for (int i = 1; i < s1.size(); i++) {
        if (s1[i - 1] != s1[i]) {
            
            *(head + index) =  '_';
            *(head + ++index) =  count + '0';
            *(head + ++index) =  '_';
            *(head + ++index) =  s1[i];
            count = 1;
        }
        else {
            count++;
        }
    }
    *(head + ++index) = '\0';
    return head;
}