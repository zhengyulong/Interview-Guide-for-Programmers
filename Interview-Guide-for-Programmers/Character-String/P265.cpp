/*
 * @Author: 缄默
 * @Date: 2022-03-16 21:55:23
 * @LastEditors: 缄默
 * @LastEditTime: 2022-03-16 22:40:13
 */

//字符串的调整与替换

#include <iostream>
#include <string>

using namespace std;

void change(string& s);
int main() {
    string s1 = "123**456";
    string s2 = "123**4**56";
    change(s1);
    change(s2);
    cout << "s1 : " << s1 << endl;
    cout << "s2 : " << s2 << endl;

    return 0;
}

void change(string& s) {
    int temp = s.size() - 1;
    for (int i = s.size() - 1; i >= 0; i--) {
        if (s[i] > '0' && s[i] < '9') {
            s[temp] = s[i];
            temp--;
        }
    }
    while (temp >= 0) {
        s[temp--] = '*';
    }
}
