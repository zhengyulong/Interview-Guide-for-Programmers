/*
 * @Author: 缄默
 * @Date: 2022-03-11 21:31:46
 * @LastEditors: 缄默
 * @LastEditTime: 2022-03-11 21:52:59
 */

#include <iostream>
#include <vector>
#include <string>
#include <map>

using namespace std;

bool isDeformation(string& s1, string& s2);

int main() {
    string s1 = "abcd";
    string s2 = "dcba";
    cout << isDeformation(s1, s2) << endl;
    return 0;
}

bool isDeformation(string& s1, string& s2) {
    map <char, int> hashMap1;
    map <char, int> hashMap2;
    for (auto ctr : s1) {
        hashMap1[ctr] += 1;
    }
    for (auto ctr : s2) {
        hashMap2[ctr] += 1;
    }
    for (auto iter = hashMap1.begin(); iter != hashMap1.end(); iter++) {
        if (iter->second != hashMap2[iter->first]){
            return false;
        }
    }
    return true;

}